// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "MyPawn.generated.h"

UCLASS()
class SPIKE_4_API AMyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		USceneComponent* OurVisibleComponent;
	
	UPROPERTY(EditAnywhere)
		UCameraComponent* OurCamera;


	// Input functions
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void Forward(float AxisValue);
	
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void Right(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void Sprint(bool NowRunning);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Sprint")
		bool IsRunning = false;

private:
	FVector CurrentVelocity;

	float DesiredForwardSpeed;
	float DesiredRightSpeed;

	void UpdateVelocity();
};
