# Spike Report

## SPIKE 4

### Introduction

The purpose of this spike was to learn how to use C++ and Blueprints together, and looking across the entirety of how Unreal Engine works to understand it.
We needed to build a small project in Unreal that explores each of these areas and utilises C++.

### Goals

1. A small Unreal Engine project, built from scratch (can use content from any project from the Learn Tab in the Epic Games Launcher).
    - Any small game which you like, manage the scope yourself.
    - I would recommend finding a classic Arcade game that you want to copy.
    - The project must include the following custom classes (i.e. child classes):
1. Game Mode (using Blueprint or C++)
    - Sets the game to use the Pawn, Controller and GameState below
1. A Pawn or Character (C++)
    - Game Logic for any playable/controllable Actors goes here
    - There must be at least one UFUNCTION which the Controller can call
    - There must be at least one UPROPERTY which the Controller can read or write to
1. A Player Controller (Blueprints or C++)
    - This must use the C++ functions and variables from the Pawn/Character
    - Control logic goes here (i.e. how to convert Input for the Pawn to use)
    - It must support at LEAST: Mouse and Keyboard
    - Should also support one of:
        - Game Pad
        - Virtual Reality Controllers
1. A GameState (C++)
    - It should track a Score of some kind (or Timer, or similar)


### Personnel

* Primary - Mitchell Bailey
* Secondary - Bradley Eales

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [Player Input and Pawns](https://docs.unrealengine.com/latest/INT/Programming/Tutorials/PlayerInput/index.html)
* [Intro to Unreal C++](https://docs.unrealengine.com/latest/INT/Programming/Introduction/index.html)

### Tasks Undertaken

1. Created a new blank C++ Unreal Project.
1. Set up game inputs.
    - WASD for movement, Left Shift for sprint.
1. Created a C++ pawn class.
    - Added a UPROPERTY Boolean for running state.
    - Created 3 float variables that focus on player velocity.
        - CurrentVelocity 
        - DesiredForwardSpeed
        - DesiredRightSpeed
    - Enabled BeginPlay and Tick functions.
        - Tick will handle pawn velocity.
    - Implemented basic movement functions as outlined in Player Input and Pawns.
        - Velocity and Boolean check for sprint.
1. Created a Player Controller Blueprint Class that called functions from the pawn using previously set up game inputs.
1. Created a C++ Game State that counted down from 30 using BeginPlay and Tick.
    - Called Tick function.
    - BeginPlay called, OnScreenDebug used to print time to screen, updated through Tick.
    - When time reaches 0, close the game.
1. Created a Game Mode that recognises the pawn, player controller, and game state.
1. Changed game mode in project settings to the new game mode created in the last step.

### What we found out

- How to use C++ in Unreal.
- How to use a Player Controller effectively in Unreal.
- How Game States work.